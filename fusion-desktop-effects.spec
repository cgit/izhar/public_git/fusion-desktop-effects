# sitelib for noarch packages, sitearch for others (remove the unneeded one)
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           fusion-desktop-effects
Version:        0.0.1
Release:        1%{?dist}
Summary:        Desktop Effects dialog for Compiz Fusion

Group:          User Interface/Desktops
License:        GPLv2
URL:            http://www.compiz-fusion.org

Source0:        http://izhar.fedorapeople.org/%{name}/%{name}-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel desktop-file-utils
Requires:       compiz-gnome compizconfig-python

%description
Compiz Fusion equivalent for Desktop Effects dialog in Fedora
mainline Compiz. This tool starts Compiz with CCP plugin
and also set it to start on login.

%prep
%setup -q -n %{name}

%build
# Remove CFLAGS=... for noarch packages (unneeded)
%{__python} setup.py build --prefix %{_prefix}

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT --prefix %{_prefix}
desktop-file-install \
      --dir=${RPM_BUILD_ROOT}%{_datadir}/applications \
      --delete-original \
      $RPM_BUILD_ROOT%{_datadir}/applications/compiz-fusion-gtk.desktop

desktop-file-install --vendor="fedora" \
      --dir=${RPM_BUILD_ROOT}%{_datadir}/applications \
      --delete-original \
      $RPM_BUILD_ROOT%{_datadir}/applications/fusion-desktop-effects.desktop

%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc COPYING AUTHORS
%{_bindir}/compiz-fusion-gtk
%{_bindir}/fusion-desktop-effects
%{_datadir}/applications/compiz-fusion-gtk.desktop
%{_datadir}/applications/fedora-fusion-desktop-effects.desktop
%{python_sitelib}/fusion_desktop_effects-%{version}-py?.?.egg-info
%{_datadir}/%{name}/


%changelog
* Thu Dec 26 2008 Mohd Izhar Firdaus Ismail <mohd.izhar.firdaus@gmail.com> 0.0.1-1
- Initial Package
