#
# fusion-desktop-effects Language File
#
msgid ""
msgstr ""
"Project-Id-Version: fusion-desktop-effects 0.0.1\n"
"POT-Creation-Date: 2008-12-26 12:05:06.836278\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: utf-8\n"

msgid "Compiz Fusion"
msgstr ""

msgid "Enable Compiz Fusion Desktop Effects"
msgstr ""

msgid "<b>Desktop Effects</b>"
msgstr ""

msgid "Keep Settings"
msgstr ""

msgid "<b>Do you want to keep these settings?</b>"
msgstr ""

msgid "Testing the new settings. If you don't respond in 30 seconds, the previous settings will be restored."
msgstr ""

msgid "Use Previous Settings"
msgstr ""

msgid "Select Compiz Fusion"
msgstr ""

msgid "_Expert"
msgstr ""

msgid "_Advanced"
msgstr ""
