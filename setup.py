#!/usr/bin/env python

import sys, subprocess, os
from distutils.core import setup
from distutils.command.build import build as _build

prefix="/usr"
pyscripts = ["fusion-desktop-effects"]
scripts = ["compiz-fusion-gtk"]
desktopfiles = ['compiz-fusion-gtk.desktop','fusion-desktop-effects.desktop']
podir = "po/"

mofiles = []

if os.path.isdir (podir):
    mopath = "build/locale/%s/fusion-desktop-effects.mo"
    destpath = "share/locale/%s/LC_MESSAGES"
    for name in os.listdir (podir):
        if name[-2:] == "po":
            name = name[:-3]
            mofiles.append ((destpath % name, [mopath % name]))


def simple_getopt(l,var):
    idx = 0
    for i in l:
        v = "%s=" % var
        if v in i:
           return (var,i.replace(v,''),idx)
        if i == var:
           return (var,l[idx+1],idx)
        else:
           idx += 1
    return (None,None,idx)

if len(sys.argv) > 2:
   o,v,i = simple_getopt(sys.argv,'--prefix')
   if v:
      prefix = v
      if sys.argv[1] == "build":
         if "%s=%s" % (o,v) in sys.argv:
            sys.argv.remove("%s=%s" % (o,v))
         else:
            sys.argv.pop(i)
            sys.argv.pop(i)

datadir="%s/share/fusion-desktop-effects/" % prefix



class build(_build):
   def run(self):
       # build python scripts
       for s in pyscripts:
           f = open("%s.in" % s).read().replace("@prefix@",prefix).replace("@datadir@",datadir)
           fo = open(s,'w')
           fo.write(f)
           fo.close()

       # build .desktop files
       for d in desktopfiles:
           cmd = "intltool-merge -d -u po/ %s.in %s" % (d,d)
           p = subprocess.Popen(cmd.split(' '))
           p.wait()

       podir = os.path.join (os.path.realpath ("."), "po")
       if os.path.isdir (podir):
           buildcmd = "msgfmt -o build/locale/%s/fusion-desktop-effects.mo po/%s.po"
           mopath = "build/locale/%s/fusion-desktop-effects.mo"
           destpath = "share/locale/%s/LC_MESSAGES"
           for name in os.listdir (podir):
               if name[-2:] == "po":
                   name = name[:-3]
                   if not os.path.isdir ("build/locale/" + name):
                          os.makedirs ("build/locale/" + name)
                   subprocess.Popen(buildcmd % (name, name),shell=True)
       _build.run(self)



setup (
        name             = "fusion-desktop-effects",
        version          = "0.0.1",
        description      = "Compiz Fusion Desktop Effects",
        author           = "Mohd Izhar Firdaus Ismail",
        author_email     = "izhar@fedoraproject.org",
        url              = "http://blog.kagesenshi.org/",
        license          = "GPL",
        data_files       = [
                            (datadir,['images/cf_logo.png']),
                            ('%s/share/applications' % prefix, desktopfiles)
                           ] + mofiles,
        packages         = [],
        scripts          = scripts + pyscripts,
        cmdclass         = {"build":build},
     )


